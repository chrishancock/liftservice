<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Servlet Hello World</title>
</head>
<body>
	This is the control panel<br/>

	<c:forEach items="${panelInfos}" var="panelInfo">
		<c:choose>
			<c:when test="${!panelInfo.getRequestedCome()}">
				<form id="floor${panelInfo.getFloorNumber()}Form" name="submitForm" method="POST" action="/liftservice/controlPanel">
					<input type="hidden" name="liftRequest" value="requestCome">
					<input type="hidden" name="floor" value="${panelInfo.getFloorNumber()}">
					<div onClick="document.getElementById('floor${panelInfo.getFloorNumber()}Form').submit()">
						request to go to floor ${panelInfo.getFloorName()}</div>
				</form>
			</c:when>
			<c:otherwise>
				<div>requested to go to floor ${panelInfo.getFloorName()}</div>
			</c:otherwise>
		</c:choose>


	</c:forEach>
</body>
</html>