<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>lift front page</title>
</head>
<body>

	Hello, the control panel is <a href="./controlPanel">here</a>, and here are user panels for respective floors:
	
	<table>
	<c:forEach items="${panelInfos}" var="panelInfo">
		<tr>
			<td>${panelInfo.getFloorNumber()}:    <a href="${panelInfo.getUrl()}">Floor ${panelInfo.getFloorName()}</a></td>
		</tr>
	</c:forEach>
	</table>
</body>
</html>