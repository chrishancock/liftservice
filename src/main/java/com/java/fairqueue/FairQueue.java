package com.java.fairqueue;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/*
 * @Author: CHancock
 * 
 * Please note, methods such as #addAll that operate on collections are not atomic;
 * They are composed of multiple calls that lock individually, but release between calls.
 */
/**
 * The Class FairQueue.
 * 
 * @param <E>
 *            the element type
 */
public final class FairQueue<E> extends AbstractQueue<E> implements
		BlockingQueue<E> {

	ReentrantLock fairLock = new ReentrantLock(true);
	Condition itemRemoved = fairLock.newCondition();
	Condition itemAdded = fairLock.newCondition();

	/** The max length of the queue. */
	private int maxLength;

	/** The queue data. */
	private Object[] queueData;

	/** The index of the next array location item to pop from. */
	private int nextPopIndex;

	/** The index of the next array location to push to. */
	private  int nextPushIndex;

	/** The current length of the queue. */
	private volatile int currentLength;

	/**
	 * Instantiates a new fair queue, a queue that serves consumers in
	 * request-order.
	 * 
	 * @param maxLength
	 *            the max length
	 */
	public FairQueue(int maxLength) {
		this.maxLength = maxLength;
		this.queueData = new Object[maxLength];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#put(java.lang.Object)
	 */
	public void put(E e) throws InterruptedException {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			while (currentLength == this.maxLength) {
				itemRemoved.await();
			}

			queueData[nextPushIndex] = e;

			// wrap-round if we've hit the end of the array
			if (++nextPushIndex == maxLength) {
				nextPushIndex = 0;
			}
			currentLength++;
			
			itemAdded.signal();
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Queue#offer(java.lang.Object)
	 */
	public boolean offer(E e) {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			if (currentLength == this.maxLength) {
				return false;
			}

			queueData[nextPushIndex] = e;

			// wrap-round if we've hit the end of the array
			if (++nextPushIndex == maxLength) {
				nextPushIndex = 0;
			}
			currentLength++;
			
			itemAdded.signal();
		} finally {
			fairLock.unlock();
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#offer(java.lang.Object, long,
	 * java.util.concurrent.TimeUnit)
	 */
	public boolean offer(E e, long timeout, TimeUnit unit){
	fairLock.lock(); // block if needed, then acquire and continue
	try {
		boolean timedOut;
		while (currentLength == this.maxLength) {
			try {
				timedOut = !itemRemoved.await(timeout, unit);
			} catch (InterruptedException ie) {
				return false;
			}
			
			if(timedOut){
				return false;
             }
		}

		queueData[nextPushIndex] = e;

		// wrap-round if we've hit the end of the array
		if (++nextPushIndex == maxLength) {
			nextPushIndex = 0;
		}
		currentLength++;
		
		itemAdded.signal();
	} finally {
		fairLock.unlock();
	}
	return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#take()
	 */
	@SuppressWarnings("unchecked")
	public E take() throws InterruptedException {
		E popped;
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			if (currentLength == 0) {
				itemAdded.await();
			}

			popped = (E) queueData[nextPopIndex];
			queueData[nextPopIndex] = null; // release the object reference

			// wrap-round if we've hit the end of the array
			if (++nextPopIndex == maxLength) {
				nextPopIndex = 0;
			}
			currentLength--;
			
			itemRemoved.signal();
		} finally {
			fairLock.unlock();
		}
		return popped;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Queue#poll()
	 */
	@SuppressWarnings("unchecked")
	public E poll() {
		E popped = null;
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			if (currentLength > 0) {
				popped = (E) queueData[nextPopIndex];
				queueData[nextPopIndex] = null; // release the object reference

				// wrap-round if we've hit the end of the array
				if (++nextPopIndex == maxLength) {
					nextPopIndex = 0;
				}
				currentLength--;
				
				itemRemoved.signal();
			}
		} finally {
			fairLock.unlock();
		}
		return popped;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#poll(long,
	 * java.util.concurrent.TimeUnit)
	 */
	@SuppressWarnings("unchecked")
	public E poll(long timeout, TimeUnit unit) throws InterruptedException {
		E popped;
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			while (currentLength == 0) {
				if (!itemAdded.await(timeout, unit)){
					return null;
				}
			}

			popped = (E) queueData[nextPopIndex];
			queueData[nextPopIndex] = null; // release the object reference

			// wrap-round if we've hit the end of the array
			if (++nextPopIndex == maxLength) {
				nextPopIndex = 0;
			}
			currentLength--;
			
			itemRemoved.signal();
		} finally {
			fairLock.unlock();
		}
		return popped;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Queue#peek()
	 */
	@SuppressWarnings("unchecked")
	public E peek() {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			if (currentLength == 0) {
				return null;
			}
			return (E) queueData[nextPopIndex];
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#size()
	 */
	public int size() {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			return this.currentLength;
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#remainingCapacity()
	 */
	public int remainingCapacity() {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			return this.maxLength - this.currentLength;
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#remove(java.lang.Object)
	 */
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Checking for specific objects in FairQueue not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#contains(java.lang.Object)
	 */
	public boolean contains(Object o) {
		throw new UnsupportedOperationException("Checking for specific objects in FairQueue not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#toArray()
	 */
	public Object[] toArray() {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			return super.toArray();
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#toArray(java.lang.Object[])
	 */
	public <T> T[] toArray(T[] a) {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			return super.toArray(a);
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractQueue#clear()
	 */
	public void clear() {
		fairLock.lock(); // block if needed, then acquire and continue
		try {
			super.clear();
		} finally {
			fairLock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#drainTo(java.util.Collection)
	 */
	public int drainTo(Collection<? super E> c) {
		throw new UnsupportedOperationException("Draining to FairQueue not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.BlockingQueue#drainTo(java.util.Collection,
	 * int)
	 */
	public int drainTo(Collection<? super E> c, int maxElements) {
		throw  new UnsupportedOperationException("Draining to FairQueue not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#iterator()
	 */
	public Iterator<E> iterator() {
		throw  new UnsupportedOperationException("Iterating over FairQueue not supported");
	}
}
