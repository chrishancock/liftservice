package com.java.fairqueue;

public class QueueConsumer implements Runnable{
	private final FairQueue<String> queue;
	private String name;
	
	public QueueConsumer(FairQueue<String> queue, String name){
		this.queue = queue;
		this.name = name;
	}
	
	public void consume(){
		new Thread(this).start();
	}

	public void run() {
		System.out.println(String.format("%s requesting", name));
		try {
			queue.take();
		} catch (InterruptedException e) {
			System.out.println("Couldn't consume, exception thrown!");
			e.printStackTrace();
			return;
		}

        System.out.println(String.format("%s received", this.name));
	}
}
