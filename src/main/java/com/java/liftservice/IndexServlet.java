package com.java.liftservice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="Controller", urlPatterns={"/"})

public class IndexServlet extends HttpServlet {
	private static final int CONTROL_LIFT_SIZE = 15;
	private static final String CONTROL_LIFT_NAME = "MyLift";

	private static final long serialVersionUID = 973143523420078010L;

	PanelManager panelManager = PanelManager.createPanelManager(CONTROL_LIFT_NAME, CONTROL_LIFT_SIZE);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("panelInfos", panelManager.getPanelInfos());
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request,
				response);
	}
}