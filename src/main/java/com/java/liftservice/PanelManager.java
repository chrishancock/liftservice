package com.java.liftservice;

import java.util.HashMap;
import java.util.Map;

public final class PanelManager {
	private final static int MAX_FLOORS = 1000;
	private final static Map<String, PanelManager> panelManagers = new HashMap<String, PanelManager>();

	private final int numberOfFloors;
	private final PanelInfo[] panelInfos;
	private final LiftController liftController;

	private PanelManager(int numberOfFloors) {
		this.liftController = new LiftController(numberOfFloors);
		
		this.numberOfFloors = numberOfFloors;

		if (numberOfFloors < 0) {
			throw new IllegalArgumentException(
					"Number of floors must be greater than zero.");
		}

		if (numberOfFloors > MAX_FLOORS) {
			throw new IllegalArgumentException("Number of floors is too high.");
		}

		panelInfos = new PanelInfo[this.numberOfFloors];
		for (int i = 0; i < numberOfFloors; ++i) {
			panelInfos[i] = new PanelInfo(this, i);
		}
	}
	
	public int getNumberOfFloors (){
		return this.numberOfFloors;
	}
	
	public void requestCome(int floor) {
		this.liftController.requestCome(floor);
	}

	public void requestUp(int floor) {
		this.liftController.requestUp(floor);
	}

	public void requestDown(int floor) {
		this.liftController.requestDown(floor);
	}

	public static PanelManager createPanelManager(String liftName, int floors) {
		if(liftName == null){
			throw new IllegalArgumentException("Lift name cannot be null");
		}
		
		if (panelManagers.containsKey(liftName)) {
			throw new IllegalArgumentException("PanelManager for list '"
					+ liftName + "' already exists.");
		}
		PanelManager newPanel = new PanelManager(floors);
		panelManagers.put(liftName, newPanel);
		return newPanel;
	}

	public static PanelManager getPanelManager(String liftName) {
		if (!panelManagers.containsKey(liftName)) {
			throw new IllegalArgumentException(
					"No PanelManager with the name '" + liftName + "' exists.");
		}
		return panelManagers.get(liftName);
	}
	
	public PanelInfo getPanelInfo(int floorNumber){
		PanelInfo panel = this.panelInfos[floorNumber];
		liftController.configureRequests(panel);
		return panel;
	}
	
	public PanelInfo[] getUpdatedPanelInfos(){
		PanelInfo[] panelArray = panelInfos.clone();
		for (PanelInfo panelInfo : panelArray){
			liftController.configureRequests(panelInfo);
		}
		return panelArray;
	}
	
	public PanelInfo[] getPanelInfos(){
		return panelInfos.clone();
	}

	public final class PanelInfo {
		private static final String LIFT_USER_PANEL_URL = "./userPanel?floor=%s";
		
		private final int floorNumber;
		private final String floorName;

		boolean requestedUp;
		boolean requestedDown;
		boolean requestedCome;

		// for internal instantiation only
		private PanelInfo(PanelManager panelManager, int floorNumber) {
			this.floorNumber = floorNumber;

			if (this.floorNumber == 0) {
				this.floorName = "G";
			} else if (this.floorNumber + 1 == panelManager.numberOfFloors) {
				this.floorName = String.valueOf(this.floorNumber) + " (ROOF)";
			} else {
				this.floorName = String.valueOf(this.floorNumber);
			}
			
			panelManager.liftController.configureRequests(this);
		}
		
		public boolean getRequestedUp(){
			return this.requestedUp;
		}
		
		public boolean getRequestedDown(){
			return this.requestedDown ;
		}
		
		public boolean getRequestedCome(){
			return this.requestedCome;
		}
		
		public int getFloorNumber() {
			return this.floorNumber;
		}

		public String getFloorName() {
			return this.floorName;
		}

		
		public String getUrl(){
			return String.format(LIFT_USER_PANEL_URL, this.floorNumber);
		}

	}


}
