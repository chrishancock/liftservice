package com.java.liftservice;

import com.java.fairqueue.FairQueue;
import com.java.liftservice.PanelManager.PanelInfo;

public class LiftController {
	private final FairQueue<LiftRequest> queue;
	private final int[] upRequests;
	private final int[] downRequests;
	private final int[] comeRequests;

	public LiftController(int floors) {
		// An up/down request for each floor (minus top and bottom floors that
		// cannot go up or down respectively) and requests from within the lift
		// to go
		// to floors as well.
		int maxQueuedItems = (floors * 3) - 2;

		queue = new FairQueue<LiftRequest>(maxQueuedItems);
		// 0 is the ground floor
		upRequests = new int[floors - 1];
		// 0 is the *first* floor
		downRequests = new int[floors - 1];
		comeRequests = new int[floors];

		new Thread(new LiftSim()).start();
	}

	public void requestCome(int floor) {
		new Thread(new RequestMaker(new ComeRequest(floor))).start();
	}

	public void requestUp(int floor) {
		new Thread(new RequestMaker(new UpRequest(floor))).start();
	}

	public void requestDown(int floor) {
		new Thread(new RequestMaker(new DownRequest(floor))).start();
	}

	public void configureRequests(PanelInfo panelInfo) {
		int panelFloor = panelInfo.getFloorNumber();

		panelInfo.requestedCome = comeRequests[panelFloor] > 0;
		if (panelFloor < upRequests.length) {
			panelInfo.requestedUp = upRequests[panelFloor] > 0;
		}
		if (panelFloor > 0) {
			panelInfo.requestedDown = downRequests[panelFloor - 1] > 0;
		}
	}

	private class RequestMaker implements Runnable {
		private LiftRequest request;

		private RequestMaker(LiftRequest request) {
			this.request = request;
		}

		public void run() {
			try {
				queue.put(request);
				if (request instanceof UpRequest) {
					upRequests[request.getFloor()]++;
				} else if (request instanceof DownRequest) {
					downRequests[request.getFloor() - 1]++;
				} else if (request instanceof ComeRequest) {
					comeRequests[request.getFloor()]++;
				}
			} catch (InterruptedException e) {
				System.out.println("ERROR: unable to put item on queue");
			}
		}
	}

	private class LiftSim implements Runnable {
		public void run() {
			while (true) {
				LiftRequest request;
				try {
					request = queue.take();

				} catch (InterruptedException e) {
					System.out.println("ERROR: unable to take item from queue");
					continue;
				}
				System.out.println("The lift is "
						+ request.getActionDescription());
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					System.out.println("ERROR: unable to sleep thread");
					continue;
				} finally {
					if (request instanceof UpRequest) {
						upRequests[request.getFloor()]--;
					} else if (request instanceof DownRequest) {
						downRequests[request.getFloor() - 1]--;
					} else if (request instanceof ComeRequest) {
						comeRequests[request.getFloor()]--;
					}
				}
			}
		}
	}

	private interface LiftRequest {
		int getFloor();

		String getActionDescription();
	}

	private abstract class AbstractLiftRequest implements LiftRequest {
		int floor;

		private AbstractLiftRequest(int floor) {
			this.floor = floor;
		}

		public int getFloor() {
			return this.floor;
		}
	}

	private final class UpRequest extends AbstractLiftRequest {
		private UpRequest(int floor) {
			super(floor);
		}

		public String getActionDescription() {
			return "Going up to floor " + this.floor;
		}
	}

	private final class DownRequest extends AbstractLiftRequest {
		private DownRequest(int floor) {
			super(floor);
		}

		public String getActionDescription() {
			return "Going down to floor " + this.floor;
		}
	}

	private final class ComeRequest extends AbstractLiftRequest {
		private ComeRequest(int floor) {
			super(floor);
		}

		public String getActionDescription() {
			return "Going to floor " + this.floor;
		}
	}
}
